﻿using System.Web.Optimization;

namespace SAM.TouristAgency.WEB
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js",
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                        "~/Scripts/MyScripts/LoginAndRegistration.js"));

            bundles.Add(new ScriptBundle("~/bundles/hotel").Include(
                     "~/Scripts/MyScripts/Admin/Hotel.js"));

            bundles.Add(new ScriptBundle("~/bundles/hotelimage").Include(
                       "~/Scripts/MyScripts/Admin/HotelImage.js"));

            bundles.Add(new ScriptBundle("~/bundles/tour").Include(
                     "~/Scripts/MyScripts/Admin/Tour.js"));

            bundles.Add(new ScriptBundle("~/bundles/allComment").Include(
                    "~/Scripts/MyScripts/Admin/AllComment.js"));

            bundles.Add(new ScriptBundle("~/bundles/comment").Include(
                   "~/Scripts/MyScripts/Admin/Comment.js"));

            bundles.Add(new ScriptBundle("~/bundles/usercomment").Include(
                   "~/Scripts/MyScripts/User/Comment.js"));

            bundles.Add(new ScriptBundle("~/bundles/usertour").Include(
                  "~/Scripts/MyScripts/User/Tour.js"));

            bundles.Add(new ScriptBundle("~/bundles/userorder").Include(
                  "~/Scripts/MyScripts/User/Order.js"));

            bundles.Add(new ScriptBundle("~/bundles/userhotel").Include(
                  "~/Scripts/MyScripts/User/Hotel.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
