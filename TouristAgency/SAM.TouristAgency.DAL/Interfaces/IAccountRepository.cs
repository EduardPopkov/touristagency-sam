﻿using SAM.TouristAgency.DAL.Entities;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface IAccountRepository
    {
        Account GetByEmail(string email);

        void Registration(Account entity);

        string GetRole(string email);

        bool Login(string email, string password);

        bool IsExist(string email);

        bool IsAdmin(string email);
    }
}
