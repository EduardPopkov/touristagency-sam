﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.DAL.Entities
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        public int TourId { get; set; }

        [Required]
        [StringLength(200)]
        public string Message { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }

        [Required]
        public bool IsRight { get; set; }

        public virtual Account Account { get; set; }
        public virtual Tour Tour { get; set; }
    }
}
