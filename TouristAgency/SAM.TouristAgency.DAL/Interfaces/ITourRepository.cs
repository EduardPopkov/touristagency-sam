﻿using SAM.TouristAgency.DAL.Entities;
using System.Linq;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface ITourRepository : IRepository<Tour>
    {
        IQueryable<Tour> GetTempTours(string sortType, int pageSize, int currentPage);

        void Update(Tour entity);

        bool IsExist(string name, int hotelId);
    }
}
