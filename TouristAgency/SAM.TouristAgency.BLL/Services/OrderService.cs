﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Order;
using SAM.TouristAgency.Model.ViewModels.Tour;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAM.TouristAgency.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITourService _tourService;

        public OrderService(IUnitOfWork unitOfWork, ITourService tourService)
        {
            _unitOfWork = unitOfWork;
            _tourService = tourService;
        }

        public void CancelOrder(int id)
        {
            Order order = _unitOfWork.Orders
                .GetById(id);

            if (order == null)
            {
                throw new ApplicationException($"Can't find order with id = {id}");
            }

            _unitOfWork.Orders
                .Delete(order);

            Tour tour = _unitOfWork.Tours.GetById(order.TourId);
            Hotel hotel = _unitOfWork.Hotels.GetById(tour.HotelId);

            hotel.Capacity += order.Amount;

            _unitOfWork.Save();
        }

        public IEnumerable<OrderVM> GetAllByAccountId(int accountId)
        {
            return _unitOfWork.Orders
                .GetAllByAccountId(accountId)
                .Select(order => new OrderVM
                {
                    OrderId = order.OrderId,
                    AccountId = order.AccountId,
                    Amount = order.Amount,
                    Cost = order.Cost,
                    DateOrder = order.DateOrder,
                    TourId = order.TourId
                });
        }

        public OrderVM GetOrder(int id)
        {
            Order order = _unitOfWork.Orders
                 .GetById(id);

            if (order == null)
            {
                throw new ApplicationException($"Can't find order with id = {id}");
            }

            return new OrderVM
            {
                OrderId = order.OrderId,
                AccountId = order.AccountId,
                Amount = order.Amount,
                Cost = order.Cost,
                DateOrder = order.DateOrder,
                TourId = order.TourId
            };
        }

        public void NewOrder(BookingVM model)
        {
            TourVM tour = _tourService.GetById(model.TourId);

            if (tour == null)
            {
                throw new ApplicationException($"Can't find tour with id = {model.TourId}");
            }

            Hotel hotel = _unitOfWork.Hotels
               .GetById(tour.HotelId);

            decimal cost = model.Amount * tour.HotelCapacity;

            _unitOfWork.Orders
                .Add(new Order
                {
                    Cost = cost,
                    TourId = model.TourId,
                    AccountId = model.AccountId,
                    Amount = model.Amount,
                    CreditCard = model.CreditCard,
                    DateIssued = model.DateIssued,
                    DateOrder = model.DateIssued,
                    Name = model.Name,
                    Passport = model.Passport
                });

            hotel.Capacity -= model.Amount;

            _unitOfWork.Hotels.Update(hotel);

            _unitOfWork.Save();
        }
    }
}
