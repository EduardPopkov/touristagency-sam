﻿using SAM.TouristAgency.Model.ViewModels.Tour;
using System.Linq;

namespace SAM.TouristAgency.BLL.Interfaces
{
    public interface ITourService
    {
        IQueryable<TourVM> GetTempTours(string sortType, int pageSize, int currentPage);

        TourVM GetById(int id);

        void Create(TourVM model);

        void Delete(int id);

        int Count();
    }
}
