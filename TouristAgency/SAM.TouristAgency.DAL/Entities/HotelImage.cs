﻿using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.DAL.Entities
{
    public class HotelImage
    {
        [Key]
        public int ImageId { get; set; }

        [Required]
        public byte[] Image { get; set; }

        [Required]
        public int HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }
    }
}
