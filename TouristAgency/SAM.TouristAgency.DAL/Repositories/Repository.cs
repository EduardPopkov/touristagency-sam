﻿using SAM.TouristAgency.DAL.Interfaces;
using System.Data.Entity;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbSet<T> _entities;

        public Repository(DbContext context)
        {
            _entities = context.Set<T>();
        }

        public void Add(T entity)
        {
            _entities.Add(entity);
        }

        public int Count()
        {
            return _entities.Count();
        }

        public void Delete(T entity)
        {
            _entities.Remove(entity);
        }

        public IQueryable<T> GetAll()
        {
            return _entities;
        }

        public T GetById(int id)
        {
            return _entities.Find(id);
        }
    }
}
