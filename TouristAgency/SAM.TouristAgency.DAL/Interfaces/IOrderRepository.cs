﻿using SAM.TouristAgency.DAL.Entities;
using System.Collections.Generic;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<Order> GetAllByAccountId(int accountId);
    }
}
