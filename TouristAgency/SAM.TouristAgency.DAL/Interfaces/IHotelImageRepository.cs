﻿using SAM.TouristAgency.DAL.Entities;
using System.Linq;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface IHotelImageRepository : IRepository<HotelImage>
    {
        IQueryable<HotelImage> GetTempImage(string sortType, int pageSize, int currentPage);

        IQueryable<HotelImage> GetByHotelId(int hotelId);
    }
}
