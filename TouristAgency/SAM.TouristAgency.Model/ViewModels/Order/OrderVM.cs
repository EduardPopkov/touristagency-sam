﻿using System;

namespace SAM.TouristAgency.Model.ViewModels.Order
{
    public class OrderVM
    {
        public int OrderId { get; set; }

        public int Amount { get; set; }

        public decimal Cost { get; set; }

        public DateTime DateOrder { get; set; }

        public int TourId { get; set; }
        public int AccountId { get; set; }
    }
}
