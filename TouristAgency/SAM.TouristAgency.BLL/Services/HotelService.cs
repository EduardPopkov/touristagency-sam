﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Hotel;
using System;
using System.Linq;

namespace SAM.TouristAgency.BLL.Services
{
    public class HotelService : IHotelService
    {
        private readonly IUnitOfWork _unitOfWork;

        public HotelService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int Count()
        {
            return _unitOfWork.Hotels
                .Count();
        }

        public void Create(HotelVM model)
        {
            bool isExist = _unitOfWork.Hotels
                .IsExist(model.Name);

            if (isExist)
            {
                throw new ApplicationException("Hotel with the same name is already exist");
            }

            _unitOfWork.Hotels
                .Add(new Hotel
                {
                    Name = model.Name,
                    Class = model.Class,
                    Capacity = model.Capacity,
                    GeoLat = model.GeoLat,
                    GeoLong = model.GeoLong,
                    Food = model.Food
                });

            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            Hotel hotel = _unitOfWork.Hotels
                .GetById(id);

            if (hotel == null)
            {
                throw new ApplicationException($"Can't find hotel with id = {id}");
            }

            _unitOfWork.Hotels
                .Delete(hotel);

            _unitOfWork.Save();
        }

        public IQueryable<HotelComboBoxVM> GetAll()
        {
            return _unitOfWork.Hotels
                .GetAll()
                .Select(x => new HotelComboBoxVM
                {
                    HotelId = x.HotelId,
                    Name = x.Name
                });
        }

        public HotelVM GetById(int id)
        {
            Hotel hotel = _unitOfWork.Hotels
                .GetById(id);

            if (hotel == null)
            {
                throw new ApplicationException($"Can't find hotel with id = {id}");
            }

            return new HotelVM
            {
                HotelId = hotel.HotelId,
                Name = hotel.Name,
                GeoLong = hotel.GeoLong,
                GeoLat = hotel.GeoLat,
                Food = hotel.Food,
                Capacity = hotel.Capacity,
                Class = hotel.Class
            };
        }

        public IQueryable<HotelVM> GetTempHotels(string sortType, int pageSize, int currentPage)
        {
            return _unitOfWork.Hotels
                .GetTempHotels(sortType, pageSize, currentPage)
                .Select(x => new HotelVM
                {
                    HotelId = x.HotelId,
                    Capacity = x.Capacity,
                    Class = x.Class,
                    Food = x.Food,
                    GeoLat = x.GeoLat,
                    GeoLong = x.GeoLong,
                    Name = x.Name
                });
        }

        public bool IsExist(string name)
        {
            bool isExist = _unitOfWork.Hotels
                .IsExist(name);

            return isExist;
        }

        public void Update(HotelVM model)
        {
            Hotel hotel = _unitOfWork.Hotels
                 .GetById(model.HotelId);

            if (hotel == null)
            {
                throw new ApplicationException($"Can't find hotel with id = {model.HotelId}");
            }

            if (IsEqual(hotel, model))
            {
                throw new ApplicationException($"Data not changed");
            }

            _unitOfWork.Hotels
                .Update(new Hotel
                {
                    HotelId = model.HotelId,
                    Capacity = model.Capacity,
                    Class = model.Class,
                    Food = model.Food,
                    GeoLat = model.GeoLat,
                    GeoLong = model.GeoLong,
                    Name = model.Name
                });

            _unitOfWork.Save();
        }

        private bool IsEqual(Hotel hotel, HotelVM model)
        {
            if (hotel.Name.Equals(model.Name) && hotel.Capacity == model.Capacity && hotel.Class == model.Class && hotel.Food == model.Food && hotel.GeoLat == model.GeoLat && hotel.GeoLong == model.GeoLong)
            {
                return true;
            }

            return false;
        }
    }
}
