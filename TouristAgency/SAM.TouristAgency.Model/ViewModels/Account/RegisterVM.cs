﻿using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.Model.ViewModels.Account
{
    public class RegisterVM
    {
        [Required(ErrorMessage = "Email not specified")]
        [StringLength(70, ErrorMessage = "The length of the Email must not exceed 60 characters.")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Incorrect email")]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = "The length of the Password must not be less than 6 characters.", MinimumLength = 6)]
        [Required(ErrorMessage = "Password not specified")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Enter the code from the image")]
        public string Captcha { get; set; }
    }
}
