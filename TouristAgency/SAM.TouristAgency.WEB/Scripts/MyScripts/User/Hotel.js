﻿let array = new Array();
let allCount = 0;
let SearchArray = new Array();

$(document).ready(function () {
    FirstPage(SearchArray);
});

let Search = function (control, index) {
    SearchArray = new Array();

    if ($(control).val().length > 0) {

        for (let i = 0; i < array.length; i++) {
            switch (index) {
                case 0:
                    if (array[i].Name == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 1:
                    if (array[i].Class == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 2:
                    if (array[i].Food == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
            }
        }
    }
    else {
        SearchArray = array;
    }

    FirstPage(SearchArray);
}


$("#btnFirst").click(function () {
    FirstPage(SearchArray);
});

let FirstPage = function (TempArray) {
    let PageSize = 6;
    let SortType = "defaultSort";
    let CurrentPage = 1;

    $.getJSON("/Home/GetPaggedDataHotel?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = 0;
            let TotalPage = 0;
            let rowData = "";
            allCount = response.allCount;
            array = response.hotels;

            if (TempArray.length != 0) {
                if (TempArray.length <= PageSize) {
                    TotalPage = Math.ceil(allCount / PageSize);
                }
                else {
                    TotalRecords = TempArray.length;
                    TotalPage = Math.ceil(TotalRecords / PageSize);
                }

                if (PageSize > TempArray.length)
                    PageSize = TempArray.length;

                rowData = GetRows(TempArray, 0, PageSize);
            }
            else {
                TotalPage = Math.ceil(allCount / PageSize);
                rowData = GetRows(array, 0, PageSize);
            }

            $("#txbTotalPage").val(TotalPage);
            $("#HotelCard").empty();
            $("#HotelCard").append(rowData);
            $("#txbCurrentPage").val("1");
        });
}

$("#btnNext").click(function () {
    NextPage();
});

let NextPage = function () {
    let PageSize = 6;
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    CurrentPage++;

    if (CurrentPage <= TotalPage) {
        $.getJSON("/Home/GetPaggedDataHotel?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.hotels;

                let rowData = GetRows(response.hotels, 0, response.hotels.length);

                $("#HotelCard").empty();
                $("#HotelCard").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);

            });
    }
}

$("#btnPre").click(function () {
    PreviousPage();
});

let PreviousPage = function () {
    let PageSize = 6;
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();

    CurrentPage--;

    if (CurrentPage >= 1) {
        $.getJSON("/Home/GetPaggedDataHotel?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.hotels;

                let rowData = GetRows(response.hotels, 0, response.hotels.length);

                $("#HotelCard").empty();
                $("#HotelCard").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnLast").click(function () {
    LastPage();
});

let LastPage = function () {
    let PageSize = 6;
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    if (CurrentPage != TotalPage) {
        $.getJSON("/Home/GetPaggedDataHotel?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {

                let rowData = GetRows(response.hotels, 0, response.hotels.length);

                $("#HotelCard").empty();
                $("#HotelCard").append(rowData);
                $("#txbCurrentPage").val(TotalPage);
                $("#txbTotalPage").val(TotalPage);
            });
    }
}

let GetRows = function (array, startIndex, endIndex) {
    let rowData = "";

    for (let i = startIndex; i < endIndex; i++) {
        try {
            console.log(array);

            rowData = rowData + '<div class="shadow mb-5 bg-white rounded card border-dark ">' +
                '<div class="card-header border-dark" style="height:100px;">' +
                '<h4>' + array[i].Name + '</h4>' +
                '</div>' +
                '<div class="card-body text-center">' +
                '<p class="card-text"><strong>Class:</strong> ' + array[i].Class + '★</p>' +
                '<p class="card-text"><strong>Food:</strong> ' + array[i].Food + '</p>' +
                '</div>' +
                '<div class="card-footer border-dark">' +
                '<a href="/Home/HotelDetail?hotelId=' + array[i].HotelId + '" class="btn btn-block btn-outline-warning">Detail</a>' +
                '</div>' +
                '</div>';
        }
        catch (e) {
            //console.log(e);
        }
    }

    return rowData;
}
