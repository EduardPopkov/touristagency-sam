﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Common.Constants;
using SAM.TouristAgency.Common.Crypto;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Account;
using System;

namespace SAM.TouristAgency.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public AccountVM GetByEmail(string email)
        {
            Account account = _unitOfWork.Accounts
                .GetByEmail(email);

            if (account == null)
            {
                throw new ApplicationException($"Can't find account with email = {email}");
            }

            return new AccountVM
            {
                AccountId = account.AccountId,
                Email = account.Email,
                Password = account.Password
            };
        }

        public string GetRole(string email)
        {
            Account account = _unitOfWork.Accounts
                .GetByEmail(email);

            if (account == null)
            {
                throw new ApplicationException($"Can't find account with email = {email}");
            }

            return _unitOfWork.Accounts
                .GetRole(email);
        }

        public bool IsAdmin(string email)
        {
            Account account = _unitOfWork.Accounts
                .GetByEmail(email);

            if (account == null)
            {
                throw new ApplicationException($"Can't find account with email = {email}");
            }

            return _unitOfWork.Accounts
                .IsAdmin(email);
        }

        public bool IsExist(string email)
        {
            Account account = _unitOfWork.Accounts
                .GetByEmail(email);

            if (account == null)
            {
                throw new ApplicationException($"Can't find account with email = {email}");
            }

            return _unitOfWork.Accounts
                .IsExist(email);
        }

        public LoginVM Login(LoginVM model)
        {
            string passwordHash = Crypto.Sha256(model.Password + model.Email);

            bool isExist = _unitOfWork.Accounts
                .Login(model.Email, passwordHash);

            if (!isExist)
            {
                throw new ApplicationException($"Incorrect login or password");
            }

            return model;
        }

        public void Registration(RegisterVM model)
        {
            bool isExist = _unitOfWork.Accounts.IsExist(model.Email);

            if (isExist)
            {
                throw new ApplicationException("User with the same login is already exist");
            }

            _unitOfWork.Accounts
                .Registration(new Account
                {
                    Email = model.Email,
                    Password = Crypto.Sha256(model.Password + model.Email),
                    ConfirmEmail = false,
                    Role = Role.UserRole
                });

            _unitOfWork.Save();
        }
    }
}
