﻿using NLog;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Tour;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAM.TouristAgency.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TourController : Controller
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ITourService _tourService;
        private readonly IHotelService _hotelService;

        public TourController(ITourService tourService, IHotelService hotelService)
        {
            _tourService = tourService;
            _hotelService = hotelService;
        }

        public ActionResult Index()
        {
            ViewBag.HotelList = _hotelService.GetAll();
            ViewBag.CountryList = GetCountries();

            return View();
        }

        public JsonResult GetPaggedData(string sortType, int pageSize, int currentPage)
        {
            IEnumerable<TourVM> tours = _tourService.GetTempTours(sortType, pageSize, currentPage);

            var temp = from t in tours
                       select new TourVM
                       {
                           TourId = t.TourId,
                           Cost = t.Cost,
                           Country = t.Country,
                           DateStart = t.DateStart,
                           Duration = t.Duration,
                           HotelId = t.HotelId,
                           HotelName = t.HotelName,
                           Name = t.Name,
                           Description = t.Description,
                           ImagePath = Convert.ToBase64String(t.Image)
                       };

            int allCount = _tourService.Count();

            JsonResult json = Json(new { temp, allCount }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        [HttpPost]
        public ActionResult Create(TourVM model, HttpPostedFileBase UploadImage)
        {
            if (ModelState.IsValid && UploadImage != null)
            {
                byte[] imageData = null;

                using (var binaryReader = new BinaryReader(UploadImage.InputStream))
                {
                    imageData = binaryReader.ReadBytes(UploadImage.ContentLength);
                }

                model.Image = imageData;

                try
                {
                    _tourService.Create(model);
                }
                catch (ApplicationException ex)
                {
                    logger.Error(ex.Message);
                }
            }

            return RedirectToAction("Index");
        }

        public JsonResult Delete(int id)
        {
            string message = string.Empty;

            try
            {
                _tourService.Delete(id);
                message = "Success";
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static List<string> GetCountries()
        {
            var list = CultureInfo.GetCultures(CultureTypes.SpecificCultures).
                 Select(p => new RegionInfo(p.Name).EnglishName).
                 Distinct().OrderBy(s => s).ToList();

            return list;
        }
    }
}