﻿using NLog;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Account;
using SAM.TouristAgency.Model.ViewModels.Comment;
using SAM.TouristAgency.Model.ViewModels.Hotel;
using SAM.TouristAgency.Model.ViewModels.Tour;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace SAM.TouristAgency.WEB.Controllers
{
    public class HomeController : Controller
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IHotelService _hotelService;
        private readonly IHotelImageService _hotelImageService;
        private readonly ITourService _tourService;
        private readonly IAccountService _accountService;
        private readonly ICommentService _commentService;

        public HomeController(IHotelService hotelService, IHotelImageService hotelImageService, ITourService tourService, ICommentService commentService, IAccountService accountService)
        {
            _hotelService = hotelService;
            _hotelImageService = hotelImageService;
            _tourService = tourService;
            _accountService = accountService;
            _commentService = commentService;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                string email = User.Identity.Name;

                try
                {
                    bool isAdmin = _accountService.IsAdmin(email);

                    if (isAdmin)
                    {
                        return Redirect("/Admin/Hotel/Index");
                    }
                }
                catch (ApplicationException ex)
                {
                    logger.Error(ex.Message);
                }
            }

            ViewBag.CountryList = GetCountries();

            return View();
        }

        public JsonResult GetPaggedDataTour(string sortType, int pageSize, int currentPage)
        {
            IEnumerable<TourVM> tours = _tourService.GetTempTours(sortType, pageSize, currentPage);

            var temp = from t in tours
                       select new TourVM
                       {
                           TourId = t.TourId,
                           Cost = t.Cost,
                           Country = t.Country,
                           DateStart = t.DateStart,
                           Duration = t.Duration,
                           HotelId = t.HotelId,
                           HotelName = t.HotelName,
                           Description = t.Description,
                           Name = t.Name,
                           ImagePath = Convert.ToBase64String(t.Image)
                       };

            int allCount = _tourService.Count();

            JsonResult json = Json(new { temp, allCount }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        public ActionResult HotelSearch()
        {
            ViewBag.HotelList = _hotelService.GetAll();

            return View();
        }

        public JsonResult GetPaggedDataHotel(string sortType, int pageSize, int currentPage)
        {
            IQueryable<HotelVM> hotels = _hotelService.GetTempHotels(sortType, pageSize, currentPage);

            int allCount = _hotelService.Count();

            return Json(new { hotels, allCount }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HotelDetail(int hotelId)
        {
            IQueryable<HotelImageVM> pictures = _hotelImageService.GetByHotelId(hotelId);
            ViewBag.HotelImageList = pictures;

            HotelVM hotel = _hotelService.GetById(hotelId);

            if (pictures.Count() == 0)
            {
                hotel.FlagImage = false;
            }
            else
            {
                hotel.FlagImage = true;
            }

            return View(hotel);
        }

        public ActionResult TourDetail(int tourId)
        {
            TourVM tour = _tourService.GetById(tourId);

            ViewBag.CommentList = _commentService.GetTempByTourId(tourId);

            return View(tour);
        }

        [HttpPost]
        public JsonResult AddComment(CommentVM model)
        {
            string message = "The form did not pass validation";

            if (ModelState.IsValid)
            {
                try
                {
                    model.DateAdded = DateTime.Now;

                    string email = User.Identity.Name;
                    AccountVM account = _accountService.GetByEmail(email);
                    model.AccountId = account.AccountId;

                    _commentService.Add(model);

                    message = "Your comment will appear soon";
                }
                catch (ApplicationException ex)
                {
                    message = ex.Message;
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static List<string> GetCountries()
        {
            var list = CultureInfo.GetCultures(CultureTypes.SpecificCultures).
                 Select(p => new RegionInfo(p.Name).EnglishName).
                 Distinct().OrderBy(s => s).ToList();

            return list;
        }
    }
}