﻿using System.Linq;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        T GetById(int id);

        void Add(T entity);

        void Delete(T entity);

        int Count();
    }
}
