﻿using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;

        private AccountRepository _accountRepository;
        private HotelRepository _hotelRepository;
        private HotelImageRepository _hotelImageRepository;
        private TourRepository _tourRepository;
        private OrderRepository _orderRepository;
        private CommentRepository _commentRepository;

        public UnitOfWork()
        {
            _context = new DatabaseContext();
        }

        public IAccountRepository Accounts
        {
            get
            {
                if (_accountRepository == null)
                {
                    _accountRepository = new AccountRepository(_context);
                }
                return _accountRepository;
            }
        }

        public IHotelRepository Hotels
        {
            get
            {
                if (_hotelRepository == null)
                {
                    _hotelRepository = new HotelRepository(_context);
                }
                return _hotelRepository;
            }
        }

        public IHotelImageRepository HotelImages
        {
            get
            {
                if (_hotelImageRepository == null)
                {
                    _hotelImageRepository = new HotelImageRepository(_context);
                }
                return _hotelImageRepository;
            }
        }

        public ITourRepository Tours
        {
            get
            {
                if (_tourRepository == null)
                {
                    _tourRepository = new TourRepository(_context);
                }
                return _tourRepository;
            }
        }

        public ICommentRepository Comments
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(_context);
                }
                return _commentRepository;
            }
        }

        public IOrderRepository Orders
        {
            get
            {
                if (_orderRepository == null)
                {
                    _orderRepository = new OrderRepository(_context);
                }
                return _orderRepository;
            }
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
