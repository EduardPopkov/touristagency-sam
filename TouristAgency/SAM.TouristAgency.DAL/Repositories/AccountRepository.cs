﻿using SAM.TouristAgency.Common.Constants;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly DatabaseContext _context;

        public AccountRepository(DatabaseContext context)
        {
            _context = context;
        }

        public Account GetByEmail(string email)
        {
            return _context.Accounts
                .Where(x => x.Email.Equals(email))
                .FirstOrDefault();
        }

        public string GetRole(string email)
        {
            return _context.Accounts
                .Where(x => x.Email.Equals(email))
                .FirstOrDefault()
                .Role;
        }

        public bool IsAdmin(string email)
        {
            Account account = _context.Accounts
               .Where(x => x.Email.Equals(email))
               .FirstOrDefault();

            return account.Role == Role.AdminRole ? true : false;
        }

        public bool IsExist(string email)
        {
            return _context.Accounts
                .Any(x => x.Email.Equals(email));
        }

        public bool Login(string email, string password)
        {
            return _context.Accounts
                .Any(x => x.Email.Equals(email) && x.Password.Equals(password));
        }

        public void Registration(Account entity)
        {
            _context.Accounts
               .Add(new Account
               {
                   Email = entity.Email,
                   Password = entity.Password,
                   Role = Role.UserRole
               });
        }
    }
}
