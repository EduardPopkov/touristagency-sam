﻿namespace SAM.TouristAgency.Model.ViewModels.Hotel
{
    public class HotelSearchVM
    {
        public int HotelId { get; set; }
        public string Name { get; set; }
        public int Class { get; set; }
        public int Capacity { get; set; }
        public string Food { get; set; }
    }
}
