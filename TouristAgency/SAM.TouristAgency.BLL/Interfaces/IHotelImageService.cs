﻿using SAM.TouristAgency.Model.ViewModels.Hotel;
using System.Linq;

namespace SAM.TouristAgency.BLL.Interfaces
{
    public interface IHotelImageService
    {
        IQueryable<HotelImageVM> GetTempHotels(string sortType, int pageSize, int currentPage);

        IQueryable<HotelImageVM> GetByHotelId(int hotelId);

        HotelImageVM GetById(int id);

        void Create(HotelImageVM model);

        void Delete(int id);

        int Count();
    }
}
