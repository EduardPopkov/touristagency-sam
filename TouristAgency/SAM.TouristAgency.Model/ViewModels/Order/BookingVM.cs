﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.Model.ViewModels.Order
{
    public class BookingVM
    {
        public int TourId { get; set; }
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Amount not specified")]
        public int Amount { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Passport not specified")]
        [RegularExpression(@"[A-Z]{2}[0-9]{7}", ErrorMessage = "Incorrect Passport")]
        public string Passport { get; set; }

        [Required(ErrorMessage = "DateIssued not specified")]
        public DateTime DateIssued { get; set; }

        [Required(ErrorMessage = "CreditCard not specified")]
        [RegularExpression(@"[0-9]{16}", ErrorMessage = "Incorrect CreditCard")]
        public string CreditCard { get; set; }
    }
}
