﻿using NLog;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Account;
using SAM.TouristAgency.Model.ViewModels.Order;
using SAM.TouristAgency.Model.ViewModels.Tour;
using System;
using System.Web.Mvc;

namespace SAM.TouristAgency.WEB.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    public class OrderController : Controller
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IOrderService _orderService;
        private readonly ITourService _tourService;
        private readonly IHotelService _hotelService;
        private readonly IAccountService _accountService;

        public OrderController(IOrderService orderService, ITourService tourService, IHotelService hotelService, IAccountService accountService)
        {
            _orderService = orderService;
            _tourService = tourService;
            _hotelService = hotelService;
            _accountService = accountService;
        }

        public ActionResult Booking(int tourId)
        {
            ViewBag.Tour = _tourService.GetById(tourId);

            return View();
        }

        [HttpPost]
        public ActionResult Booking(BookingVM model)
        {
            TourVM tour = _tourService.GetById(model.TourId);

            if (ModelState.IsValid)
            {
                if (model.Amount > tour.HotelCapacity)
                {
                    ViewBag.Message = "There is not enough space in the hotel";
                }
                else
                {
                    string email = User.Identity.Name;
                    AccountVM account = _accountService.GetByEmail(email);

                    model.AccountId = account.AccountId;

                    _orderService.NewOrder(model);

                    return Redirect("/User/Account/MyProfile");
                }
            }

            ViewBag.Tour = tour;

            return View(model);
        }

        public JsonResult CancelBooking(int orderId)
        {
            string message = string.Empty;

            try
            {
                _orderService.CancelOrder(orderId);
                message = "Success";
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
    }
}