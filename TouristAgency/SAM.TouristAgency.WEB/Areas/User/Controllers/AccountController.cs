﻿using NLog;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Account;
using SAM.TouristAgency.Model.ViewModels.Order;
using SAM.TouristAgency.WEB.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Web.Mvc;
using System.Web.Security;

namespace SAM.TouristAgency.WEB.Areas.User.Controllers
{
    public class AccountController : Controller
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IAccountService _accountService;
        private readonly IOrderService _orderService;

        public AccountController(IAccountService accountService, IOrderService orderService)
        {
            _accountService = accountService;
            _orderService = orderService;
        }

        public ActionResult Captcha()
        {
            string code = new Random(DateTime.Now.Millisecond).Next(1111, 9999).ToString();
            Session["code"] = code;
            CaptchaImage captcha = new CaptchaImage(code, 110, 50);

            this.Response.Clear();
            this.Response.ContentType = "image/jpeg";

            captcha.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);

            captcha.Dispose();
            return null;
        }

        public JsonResult Login(LoginVM model)
        {
            string message = "The form did not pass validation";
            string role = string.Empty;

            if (ModelState.IsValid)
            {
                try
                {
                    _accountService.Login(model);

                    message = "Success";

                    role = _accountService.GetRole(model.Email);

                    FormsAuthentication.SetAuthCookie(model.Email, true);
                }
                catch (ApplicationException ex)
                {
                    logger.Error(ex.Message);
                    message = ex.Message;
                }
            }

            return Json(new { message, role }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Registration(RegisterVM model)
        {
            string message = "The form did not pass validation";

            if (ModelState.IsValid)
            {
                if (model.Captcha != (string)Session["code"])
                {
                    message = "Text from the image entered incorrectly";
                }
                else
                {
                    try
                    {
                        _accountService.Registration(model);

                        message = "Success";

                        FormsAuthentication.SetAuthCookie(model.Email, true);
                    }
                    catch (ApplicationException ex)
                    {
                        logger.Error(ex.Message);
                        message = ex.Message;
                    }
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "User")]
        public ActionResult MyProfile()
        {
            string email = User.Identity.Name;
            AccountVM account = _accountService.GetByEmail(email);

            ViewBag.Account = account;

            IEnumerable<OrderVM> orders = _orderService.GetAllByAccountId(account.AccountId);

            return View(orders);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}