﻿
let array = new Array();
let allCount = 0;

let GetDate = function (date) {
    var date = parseInt(date.replace(/\D+/g, ""));

    date = new Date(date);

    var year = date.getFullYear();
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);

    var dateObj = { y: year, m: month, d: day };

    return dateObj;
}

$(document).ready(function () {
    FirstPage();
});

$("#btnFirst").click(function () {
    FirstPage();
});

let FirstPage = function () {
    let PageSize = 5;
    let SortType = "defauldSort";
    let CurrentPage = 1;

    $.getJSON("/Admin/Comment/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalPage = 0;
            let rowData = "";
            allCount = response.allCount;
            array = response.comments;

            TotalPage = Math.ceil(allCount / PageSize);
            rowData = GetRows(array, 0, PageSize);

            $("#txbTotalPage").val(TotalPage);
            $("#CommentTable").empty();
            $("#CommentTable").append(rowData);
            $("#txbCurrentPage").val("1");
        });
}

$("#btnNext").click(function () {
    NextPage();
});

let NextPage = function () {
    let PageSize = 5;
    let SortType = "defauldSort";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    CurrentPage++;

    if (CurrentPage <= TotalPage) {
        $.getJSON("/Admin/Comment/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.comments;

                let rowData = GetRows(response.comments, 0, response.comments.length);

                $("#CommentTable").empty();
                $("#CommentTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);

            });
    }
}

$("#btnPre").click(function () {
    PreviousPage();
});

let PreviousPage = function () {
    let PageSize = 5;
    let SortType = "defauldSort";
    let CurrentPage = $("#txbCurrentPage").val();

    CurrentPage--;

    if (CurrentPage >= 1) {
        $.getJSON("/Admin/Comment/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.comments;

                let rowData = GetRows(response.comments, 0, response.comments.length);

                $("#CommentTable").empty();
                $("#CommentTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnLast").click(function () {
    LastPage();
});

let LastPage = function () {
    let PageSize = 5;
    let SortType = "defauldSort";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    if (CurrentPage != TotalPage) {
        $.getJSON("/Admin/Comment/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + TotalPage,
            {},
            function (response) {

                let rowData = GetRows(response.comments, 0, response.comments.length);

                $("#CommentTable").empty();
                $("#CommentTable").append(rowData);
                $("#txbCurrentPage").val(TotalPage);
                $("#txbTotalPage").val(TotalPage);
            });
    }
}

$("#dropPageSize").change(function () {
    let PageSize = 5;
    let SortType = "defauldSort";
    let CurrentPage = $("#txbCurrentPage").val();

    $.getJSON("/Admin/Comment/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = response.allCount;
            let TotalPage = Math.ceil(TotalRecords / PageSize);

            if (CurrentPage > TotalPage)
                CurrentPage = TotalPage;

            let rowData = GetRows(response.comments, 0, response.comments.length);

            $("#CommentTable").empty();
            $("#CommentTable").append(rowData);
            $("#txbCurrentPage").val(CurrentPage);
            $("#txbTotalPage").val(TotalPage);
        });
});

let EditRecord = function (id) {
    let url = "/Admin/Comment/GetById?id=" + id;
    $("#ModalTitle").html("Update Comment Record");
    $("#MyModal").modal();
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            var obj = JSON.parse(data);

            $("#CommentId").val(obj.CommentId);
            $("#TourId").val(obj.TourId);
            $("#TourName").val(obj.TourName);
            $("#AccountId").val(obj.AccountId);
            $("#AccountName").val(obj.AccountName);
            $("#Message").val(obj.Message);
            $("#DateAdded").val(obj.DateAdded);
            $("#IsRight").val(obj.IsRight);
        }
    })
}

let DeleteRecord = function (CommentId) {
    $("#CommentId").val(CommentId);
    $("#DeleteConfirmation").modal("show");
}

let ConfirmDelete = function () {
    let CommentId = $("#CommentId").val();

    $.ajax({
        type: "POST",
        url: "/Admin/Comment/Delete?id=" + CommentId,
        success: function (result) {
            if (result == "Success") {
                alert(result);
                $("#DeleteConfirmation").modal("hide");
                window.location.href = "/Admin/Comment/Index";
            }
            else {
                alert(result);
            }
        }
    })
}

let GetRows = function (array, startIndex, endIndex) {
    let rowData = "";

    for (let i = startIndex; i < endIndex; i++) {
        try {

            let d = GetDate(array[i].DateAdded);

            rowData = rowData + '<tr data-rowid="' + array[i].CommentId + '">' +
                '<td>' + array[i].AccountName + '</td>' +
                '<td>' + array[i].TourName + '</td>' +
                '<td>' + array[i].Message + '</td>' +
                '<td>' + d.d + '-' + d.m + '-' + d.y + '</td>' +
                '<td><a href="#" class="btn btn-warning" onclick="EditRecord(' + array[i].CommentId + ')">Edit</a></td>' +
                '<td><a href="#" class="btn btn-danger" onclick="DeleteRecord(' + array[i].CommentId + ')">Delete</i></a></td>' +
                '</tr>';
        }
        catch (e) {
            console.log(e);
        }
    }

    return rowData;
}