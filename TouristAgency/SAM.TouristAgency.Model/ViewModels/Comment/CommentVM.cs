﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.Model.ViewModels.Comment
{
    public class CommentVM
    {
        public int CommentId { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        public int TourId { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The length of the Message must not exceed 200 characters.")]
        public string Message { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }

        public bool IsRight { get; set; }

        public string AccountName { get; set; }
        public string TourName { get; set; }
    }
}
