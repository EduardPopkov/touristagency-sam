﻿let array = new Array();
let allCount = 0;
let SearchArray = new Array();

$(document).ready(function () {
    FirstPage(SearchArray);
});

let Search = function (control, index) {
    SearchArray = new Array();

    if ($(control).val().length > 0) {

        for (let i = 0; i < array.length; i++) {
            switch (index) {
                case 0:
                    if (array[i].Name == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 1:
                    if (array[i].Class == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 2:
                    if (array[i].Food == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
            }
        }
    }
    else {
        SearchArray = array;
    }

    FirstPage(SearchArray);
}

$('th a').on('click', function () {
    let PageSize = $("#dropPageSize").val();
    let CurrentPage = $("#txbCurrentPage").val();

    $(this).text($(this).text().includes(' ▼') ?
        $(this).text().replace(" ▼", " ▲") :
        $(this).text().replace(' ▲', " ▼"));

    $.getJSON("/Admin/Hotel/GetPaggedData?sortType=" + $(this).text() + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            array = response.hotels;

            let rowData = GetRows(array, 0, array.length);

            $("#HotelTable").empty();
            $("#HotelTable").append(rowData);
        });
});

$("#btnFirst").click(function () {
    FirstPage(SearchArray);
});

let FirstPage = function (TempArray) {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = 1;

    $.getJSON("/Admin/Hotel/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = 0;
            let TotalPage = 0;
            let rowData = "";
            allCount = response.allCount;
            array = response.hotels;

            if (TempArray.length != 0) {
                if (TempArray.length <= PageSize) {
                    TotalPage = Math.ceil(allCount / PageSize);
                }
                else {
                    TotalRecords = TempArray.length;
                    TotalPage = Math.ceil(TotalRecords / PageSize);
                }

                if (PageSize > TempArray.length)
                    PageSize = TempArray.length;

                rowData = GetRows(TempArray, 0, PageSize);
            }
            else {
                TotalPage = Math.ceil(allCount / PageSize);
                rowData = GetRows(array, 0, PageSize);
            }

            $("#txbTotalPage").val(TotalPage);
            $("#HotelTable").empty();
            $("#HotelTable").append(rowData);
            $("#txbCurrentPage").val("1");
        });
}

$("#btnNext").click(function () {
    NextPage();
});

let NextPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    CurrentPage++;

    if (CurrentPage <= TotalPage) {
        $.getJSON("/Admin/Hotel/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.hotels;

                let rowData = GetRows(response.hotels, 0, response.hotels.length);

                $("#HotelTable").empty();
                $("#HotelTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);

            });
    }
}

$("#btnPre").click(function () {
    PreviousPage();
});

let PreviousPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();

    CurrentPage--;

    if (CurrentPage >= 1) {
        $.getJSON("/Admin/Hotel/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.hotels;

                let TotalRecords = response.allCount;

                let rowData = GetRows(response.hotels, 0, response.hotels.length);

                $("#HotelTable").empty();
                $("#HotelTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnLast").click(function () {
    LastPage();
});

let LastPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    if (CurrentPage != TotalPage) {
        $.getJSON("/Admin/Hotel/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + TotalPage,
            {},
            function (response) {

                let rowData = GetRows(response.hotels, 0, response.hotels.length);

                $("#HotelTable").empty();
                $("#HotelTable").append(rowData);
                $("#txbCurrentPage").val(TotalPage);
                $("#txbTotalPage").val(TotalPage);
            });
    }
}

$("#dropPageSize").change(function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();

    $.getJSON("/Admin/Hotel/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = response.allCount;
            let TotalPage = Math.ceil(TotalRecords / PageSize);

            if (CurrentPage > TotalPage)
                CurrentPage = TotalPage;

            let rowData = GetRows(response.hotels, 0, response.hotels.length);

            $("#HotelTable").empty();
            $("#HotelTable").append(rowData);
            $("#txbCurrentPage").val(CurrentPage);
            $("#txbTotalPage").val(TotalPage);
        });
});

let AddNew = function () {
    $("#myForm")[0].reset();
    $("#ModalTitle").html("Add Hotel Record");
    $("#MyModal").modal();
}

let SubmitForm = function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);

    let hotelId = form.elements["HotelId"].value;
    let name = form.elements["Name"].value;
    let clas = form.elements["Class"].value;
    let capac = form.elements["Capacity"].value;
    let geoLat = form.elements["GeoLat"].value;
    let geoLong = form.elements["GeoLong"].value;
    let food = form.elements["Food"].value;

    console.log(capac);

    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: form.action,
            data: JSON.stringify({
                hotelId: hotelId,
                name: name,
                class: clas,
                capacity: capac,
                geoLat: String(geoLat).replace(".", ","),
                geoLong: String(geoLong).replace(".", ","),
                food: food
            }),
            success: function (data) {
                alert(data.message);
                if (data.message == "Success") {
                    if (data.model.HotelId > 0) {
                        $("tr[data-rowid='" + data.model.HotelId + "']").replaceWith(row(data.model));
                        $("#MyModal").modal("hide");
                    }
                    else {
                        window.location.href = "/Admin/Hotel/Index";
                    }
                }

                $("#HotelId").val(0);
            }
        });
    }

    return false;
}

let row = function (hotel) {
    return "<tr data-rowid='" + hotel.HotelId + "'>" +
        "<td>" + hotel.Name + "</td> <td>" + hotel.Class + "</td>" +
        "<td>" + hotel.Food + "</td>" + "<td>" + hotel.Capacity + "</td>" + "<td>" + hotel.GeoLong + "</td>" + "<td>" + hotel.GeoLat + "</td>" +
        '<td><a href="#" class="btn btn-warning" onclick="EditRecord(' + hotel.HotelId + ')">Edit</a></td>' +
        '<td><a href="#" class="btn btn-danger" onclick="DeleteRecord(' + hotel.HotelId + ')">Delete</i></a></td>' +
        '</tr>';
}

let EditRecord = function (id) {
    let url = "/Admin/Hotel/GetById?id=" + id;
    $("#ModalTitle").html("Update hotel Record");
    $("#MyModal").modal();
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            var obj = JSON.parse(data);

            console.log(obj);

            $("#HotelId").val(obj.HotelId);
            $("#Name").val(obj.Name);
            $("#Class").val(obj.Class);
            $("#Capacity").val(obj.Capacity);
            $("#GeoLong").val(obj.GeoLong);
            $("#GeoLat").val(obj.GeoLat);
            $("#Food").val(obj.Food);
        }
    })
}

let DeleteRecord = function (HotelId) {
    $("#HotelId").val(HotelId);
    $("#DeleteConfirmation").modal("show");
}

let ConfirmDelete = function () {
    let HotelId = $("#HotelId").val();

    $.ajax({
        type: "POST",
        url: "/Admin/Hotel/Delete?id=" + HotelId,
        success: function (result) {
            if (result == "Success") {
                alert(result);
                $("#DeleteConfirmation").modal("hide");
                window.location.href = "/Admin/Hotel/Index";
            }
            else {
                alert(result);
            }
        }
    })
}

let GetRows = function (array, startIndex, endIndex) {
    let rowData = "";

    for (let i = startIndex; i < endIndex; i++) {
        try {
            rowData = rowData + '<tr data-rowid="' + array[i].HotelId + '">' +
                '<td>' + array[i].Name + '</td>' +
                '<td>' + array[i].Class + '</td>' +
                '<td>' + array[i].Food + '</td>' +
                '<td>' + array[i].Capacity + '</td>' +
                '<td>' + array[i].GeoLong + '</td>' +
                '<td>' + array[i].GeoLat + '</td>' +
                '<td><a href="#" class="btn btn-warning" onclick="EditRecord(' + array[i].HotelId + ')">Edit</a></td>' +
                '<td><a href="#" class="btn btn-danger" onclick="DeleteRecord(' + array[i].HotelId + ')">Delete</i></a></td>' +
                '</tr>';
        }
        catch (e) {
            console.log(e);
        }
    }

    return rowData;
}