﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Hotel;
using System;
using System.Linq;

namespace SAM.TouristAgency.BLL.Services
{
    public class HotelImageService : IHotelImageService
    {
        private readonly IUnitOfWork _unitOfWork;

        public HotelImageService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int Count()
        {
            return _unitOfWork.HotelImages
                .Count();
        }

        public void Create(HotelImageVM model)
        {
            _unitOfWork.HotelImages
                .Add(new HotelImage
                {
                    HotelId = model.HotelId,
                    Image = model.Image
                });

            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            HotelImage hotelImage = _unitOfWork.HotelImages
                .GetById(id);

            if (hotelImage == null)
            {
                throw new ApplicationException($"Can't find image with id = {id}");
            }

            _unitOfWork.HotelImages
              .Delete(hotelImage);

            _unitOfWork.Save();
        }

        public IQueryable<HotelImageVM> GetByHotelId(int hotelId)
        {
            Hotel hotel = _unitOfWork.Hotels
                   .GetById(hotelId);

            if (hotel == null)
            {
                throw new ApplicationException($"Can't find hotel with id = {hotelId}");
            }

            return _unitOfWork.HotelImages
                .GetByHotelId(hotelId)
                .Select(x => new HotelImageVM
                {
                    HotelId = x.HotelId,
                    ImageId = x.ImageId,
                    Image = x.Image
                });
        }

        public HotelImageVM GetById(int id)
        {
            HotelImage hotelImage = _unitOfWork.HotelImages
               .GetById(id);

            if (hotelImage == null)
            {
                throw new ApplicationException($"Can't find image with id = {id}");
            }

            return new HotelImageVM
            {
                ImageId = hotelImage.ImageId,
                Image = hotelImage.Image,
                HotelName = hotelImage.Hotel.Name,
                HotelId = hotelImage.HotelId
            };
        }

        public IQueryable<HotelImageVM> GetTempHotels(string sortType, int pageSize, int currentPage)
        {
            return _unitOfWork.HotelImages
            .GetTempImage(sortType, pageSize, currentPage)
            .Select(x => new HotelImageVM
            {
                ImageId = x.ImageId,
                Image = x.Image,
                HotelName = x.Hotel.Name,
                HotelId = x.HotelId
            });
        }
    }
}
