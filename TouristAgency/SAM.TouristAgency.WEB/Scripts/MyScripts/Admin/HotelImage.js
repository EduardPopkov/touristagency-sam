﻿let array = new Array();
let allCount = 0;
let SearchArray = new Array();

$(document).ready(function () {
    FirstPage(SearchArray);
});

$("#btnFirst").click(function () {
    FirstPage(SearchArray);
});

let FirstPage = function (TempArray) {
    let PageSize = $("#dropPageSize").val();
    let SortType = "defaultSort";
    let CurrentPage = 1;

    $.getJSON("/Admin/HotelImage/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = 0;
            let TotalPage = 0;
            let rowData = "";
            allCount = response.allCount;
            array = response.temp;

            if (TempArray.length != 0) {
                if (TempArray.length <= PageSize) {
                    TotalPage = Math.ceil(allCount / PageSize);
                }
                else {
                    TotalRecords = TempArray.length;
                    TotalPage = Math.ceil(TotalRecords / PageSize);
                }

                if (PageSize > TempArray.length)
                    PageSize = TempArray.length;

                rowData = GetRows(TempArray, 0, PageSize);
            }
            else {

                TotalPage = Math.ceil(allCount / PageSize);
                rowData = GetRows(array, 0, PageSize);

            }

            $("#txbTotalPage").val(TotalPage);
            $("#HotelImageTable").empty();
            $("#HotelImageTable").append(rowData);
            $("#txbCurrentPage").val("1");
        });
}

$("#btnNext").click(function () {
    NextPage();
});

let NextPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    CurrentPage++;

    if (CurrentPage <= TotalPage) {
        $.getJSON("/Admin/HotelImage/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.temp;

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#HotelImageTable").empty();
                $("#HotelImageTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnPre").click(function () {
    PreviousPage();
});

let PreviousPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();

    CurrentPage--;

    if (CurrentPage >= 1) {
        $.getJSON("/Admin/HotelImage/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.temp;

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#HotelImageTable").empty();
                $("#HotelImageTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnLast").click(function () {
    LastPage();
});

let LastPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    if (CurrentPage != TotalPage) {
        $.getJSON("/Admin/HotelImage/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + TotalPage,
            {},
            function (response) {

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#HotelImageTable").empty();
                $("#HotelImageTable").append(rowData);
                $("#txbCurrentPage").val(TotalPage);
                $("#txbTotalPage").val(TotalPage);
            });
    }
}

$("#dropPageSize").change(function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "defaultSort";
    let CurrentPage = $("#txbCurrentPage").val();

    $.getJSON("/Admin/HotelImage/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = response.allCount;
            let TotalPage = Math.ceil(TotalRecords / PageSize);

            if (CurrentPage > TotalPage)
                CurrentPage = TotalPage;

            let rowData = GetRows(response.temp, 0, response.temp.length);

            $("#HotelImageTable").empty();
            $("#HotelImageTable").append(rowData);
            $("#txbCurrentPage").val(CurrentPage);
            $("#txbTotalPage").val(TotalPage);
        });
});

let GetRows = function (array, startIndex, endIndex) {
    let rowData = "";
    let imagePath = "";

    for (let i = startIndex; i < endIndex; i++) {
        try {
            imagePath = "data:image/jpeg;base64," + array[i].ImagePath;

            rowData = rowData + '<tr data-rowid="' + array[i].ImageId + '">' +
                '<td><img width="250" height="200" src="' + imagePath + '"/></td>' +
                '<td>' + array[i].HotelName + '</td>' +
                '<td><a href="#" class="btn btn-danger" onclick="DeleteRecord(' + array[i].ImageId + ')">Delete</a></td>' +
                '</tr>';
        }
        catch (e) {
        }
    }

    return rowData;
}

let AddNew = function () {
    $("#myForm")[0].reset();
    $("#MyModal").modal();
}

let DeleteRecord = function (ImageId) {
    $("#ImageId").val(ImageId);
    $("#DeleteConfirmation").modal("show");
}

let ConfirmDelete = function () {
    let ImageId = $("#ImageId").val();

    $.ajax({
        type: "POST",
        url: "/Admin/HotelImage/Delete?id=" + ImageId,
        success: function (result) {
            if (result == "Success") {
                alert(result);
                $("#DeleteConfirmation").modal("hide");
                window.location.href = "/Admin/HotelImage/Index";
            }
            else {
                alert(result);
            }
        }
    })
}

