﻿let LoginFormShow = function () {
    $("#errorDivL").hide();
    $("#LoginEmail").val("");
    $("#LoginPassword").val("");
    $("#LoginModal").modal();
}

let RegisterFormShow = function () {
    $("#errorDivR").hide();
    $("#RegisterEmail").val("");
    $("#RegisterPassowrd").val("");
    $("#RegisterConfirmPassowrd").val("");
    $("#Captcha").val("");
    $("#RegisterModal").modal();
}

let showError = function (container, errorMessage) {
    container.className = 'error';
    var msgElem = document.createElement('small');
    msgElem.className = "error-message";
    msgElem.innerHTML = errorMessage;
    msgElem.style.color = 'red';
    container.appendChild(msgElem);
}

let resetError = function (container) {
    container.className = '';
    if (container.lastChild.className == "error-message") {
        container.removeChild(container.lastChild);
    }
}

let validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

let validateLogin = function (form) {
    var elems = form.elements;

    resetError(elems.LoginEmail.parentNode);
    if (!validateEmail(elems.LoginEmail.value)) {
        showError(elems.LoginEmail.parentNode, 'Incorrect email');
    }

    resetError(elems.LoginPassword.parentNode);
    if (!elems.LoginPassword.value) {
        showError(elems.LoginPassword.parentNode, 'Password not specified');
    }

    if (validateEmail(elems.LoginEmail.value) && elems.LoginPassword.value) {
        $.ajax({
            type: "POST",
            url: form.action,
            dataType: "json",
            data: {
                email: elems.LoginEmail.value,
                password: elems.LoginPassword.value
            },
            success: function (data) {
                if (data.message == "Success") {
                    if (data.role == "Admin") {
                        window.location.href = "/Admin/Hotel/Index";
                    }
                    else {
                        window.location.href = "/Home/Index";
                    }
                }
                else {
                    $("#errorDivL").empty();
                    $('<p>' + data.message + '</p>').appendTo('#errorDivL');
                    $("#errorDivL").show();
                }
            }
        });
    }
}

let validateRegister = function (form) {
    let elems = form.elements;

    resetError(elems.RegisterEmail.parentNode);
    if (!validateEmail(elems.RegisterEmail.value)) {
        showError(elems.RegisterEmail.parentNode, 'Incorrect email');
    }

    resetError(elems.RegisterPassowrd.parentNode);
    if (!elems.RegisterPassowrd.value) {
        showError(elems.RegisterPassowrd.parentNode, 'Password not specified');
    } else if (elems.RegisterConfirmPassowrd.value != elems.RegisterPassowrd.value) {
        showError(elems.RegisterPassowrd.parentNode, 'Passwords do not match');
    }

    resetError(elems.Captcha.parentNode);
    if (!elems.Captcha.value) {
        showError(elems.Captcha.parentNode, 'Captcha not specified');
    }

    if (validateEmail(elems.RegisterEmail.value) && elems.RegisterPassowrd.value && (elems.RegisterConfirmPassowrd.value == elems.RegisterPassowrd.value) && elems.Captcha.value) {
        $.ajax({
            type: "POST",
            url: form.action,
            dataType: "json",
            data: {
                email: elems.RegisterEmail.value,
                password: elems.RegisterPassowrd.value,
                confirmPassword: elems.RegisterConfirmPassowrd.value,
                captcha: elems.Captcha.value
            },
            success: function (data) {
                if (data == "Success") {
                    window.location.href = "/Home/Index";
                }
                else {
                    console.log(data);
                    $('<p>' + data + '</p>').appendTo('#errorDivR');
                    $("#errorDivR").show();
                }
            }
        });
    }
}