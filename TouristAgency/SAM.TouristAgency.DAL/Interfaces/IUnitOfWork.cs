﻿namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IAccountRepository Accounts { get; }
        IHotelRepository Hotels { get; }
        IHotelImageRepository HotelImages { get; }
        ITourRepository Tours { get; }
        ICommentRepository Comments { get; }
        IOrderRepository Orders { get; }

        int Save();
    }
}
