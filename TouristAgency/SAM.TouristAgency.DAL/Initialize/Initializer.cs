﻿using SAM.TouristAgency.Common.Constants;
using SAM.TouristAgency.Common.Crypto;
using SAM.TouristAgency.DAL.Entities;
using System.Collections.Generic;
using System.Data.Entity;

namespace SAM.TouristAgency.DAL.Initialize
{
    public class Initializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            IList<Account> defaultUsers = new List<Account>();
            defaultUsers.Add(new Account() { Email = "admin@mail.ru", Password = Crypto.Sha256("admin" + "admin@mail.ru"), Role = Role.AdminRole });
            context.Accounts.AddRange(defaultUsers);

            IList<Hotel> defaultHotels = new List<Hotel>();
            defaultHotels.Add(new Hotel() { Name = "Dunes Village", Capacity = 1200, Class = 3, GeoLat = 23.14M, GeoLong = 26.21M, Food = Food.AI });
            defaultHotels.Add(new Hotel() { Name = "Sea Mist", Capacity = 700, Class = 2, GeoLat = 26.63M, GeoLong = 64.21M, Food = Food.RO });
            defaultHotels.Add(new Hotel() { Name = "Compass Cove", Capacity = 1400, Class = 4, GeoLat = 27.53M, GeoLong = 26.21M, Food = Food.HB });
            defaultHotels.Add(new Hotel() { Name = "Breakers Resort Hotel", Capacity = 2000, Class = 5, GeoLat = 42.14M, GeoLong = 73.75M, Food = Food.BB });
            defaultHotels.Add(new Hotel() { Name = "Coral Beach Resort Myrtle Beach", Capacity = 900, Class = 3, GeoLat = 56.35M, GeoLong = 32.64M, Food = Food.BB });
            defaultHotels.Add(new Hotel() { Name = "Dayton House Resort", Capacity = 6500, Class = 3, GeoLat = 41.14M, GeoLong = 57.67M, Food = Food.AI });
            defaultHotels.Add(new Hotel() { Name = "Patricia Grand Resort Hotel", Capacity = 500, Class = 2, GeoLat = 45.14M, GeoLong = 32.23M, Food = Food.RO });
            defaultHotels.Add(new Hotel() { Name = "DoubleTree Resort by Hilton Myrtle Beach Oceanfront", Capacity = 850, Class = 3, GeoLat = 23.25M, GeoLong = 32.21M, Food = Food.AI });
            defaultHotels.Add(new Hotel() { Name = "Caribbean Resort Myrtle Beach", Capacity = 1000, Class = 4, GeoLat = 23.14M, GeoLong = 75.74M, Food = Food.FD });
            defaultHotels.Add(new Hotel() { Name = "Sea Crest Oceanfront Resort", Capacity = 1120, Class = 4, GeoLat = 23.14M, GeoLong = 62.53M, Food = Food.FD });
            context.Hotels.AddRange(defaultHotels);

            context.SaveChanges();
        }
    }
}
