﻿using SAM.TouristAgency.DAL.Entities;
using System.Linq;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IQueryable<Comment> GetTempByTourId(int tourId);

        IQueryable<Comment> GetTempAllComments(string sortType, int pageSize, int currentPage);

        IQueryable<Comment> GetTempIsNotRightComments(string sortType, int pageSize, int currentPage);

        void Update(Comment entity);
    }
}
