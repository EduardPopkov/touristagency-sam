﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SAM.TouristAgency.DAL.Entities
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("SAMTouristAgency")
        {

        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<HotelImage> HotelImages { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
