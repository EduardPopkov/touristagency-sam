﻿using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class HotelRepository : Repository<Hotel>, IHotelRepository
    {
        private readonly DatabaseContext _context;

        public HotelRepository(DatabaseContext context)
            : base(context)
        {
            _context = context;
        }

        public IQueryable<Hotel> GetTempHotels(string sortType, int pageSize, int currentPage)
        {
            IQueryable<Hotel> hotels = null;

            switch (sortType)
            {
                case "Name ▼":
                    hotels = _context.Hotels.OrderBy(x => x.Name)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Name ▲":
                    hotels = _context.Hotels.OrderByDescending(x => x.Name)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Class ▼":
                    hotels = _context.Hotels.OrderByDescending(x => x.Class)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Class ▲":
                    hotels = _context.Hotels.OrderBy(x => x.Class)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Capacity ▼":
                    hotels = _context.Hotels.OrderByDescending(x => x.Capacity)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Capacity ▲":
                    hotels = _context.Hotels.OrderBy(x => x.Capacity)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Food ▼":
                    hotels = _context.Hotels.OrderBy(x => x.Food)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                case "Food ▲":
                    hotels = _context.Hotels.OrderByDescending(x => x.Food)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
                default:
                    hotels = _context.Hotels.OrderBy(x => x.Name)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    break;
            }

            return hotels;
        }

        public bool IsExist(string name)
        {
            return _context.Hotels
              .Any(x => x.Name.Equals(name));
        }

        public void Update(Hotel entity)
        {
            Hotel hotel = _context.Hotels
               .Where(x => x.HotelId == entity.HotelId)
               .FirstOrDefault();

            hotel.Name = entity.Name;
            hotel.Class = entity.Class;
            hotel.Capacity = entity.Capacity;
            hotel.Food = entity.Food;
            hotel.GeoLat = entity.GeoLat;
            hotel.GeoLong = entity.GeoLong;
        }
    }
}
