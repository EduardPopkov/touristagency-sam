﻿using Newtonsoft.Json;
using NLog;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Hotel;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SAM.TouristAgency.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HotelController : Controller
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IHotelService _hotelService;

        public HotelController(IHotelService hotelService)
        {
            _hotelService = hotelService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetPaggedData(string sortType, int pageSize, int currentPage)
        {
            IQueryable<HotelVM> hotels = _hotelService.GetTempHotels(sortType, pageSize, currentPage);

            int allCount = _hotelService.Count();

            return Json(new { hotels, allCount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetById(int id)
        {
            string message = string.Empty;

            try
            {
                HotelVM hotel = _hotelService.GetById(id);

                message = JsonConvert.SerializeObject(hotel, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddOrEdit(HotelVM model)
        {
            string message = "The form did not pass validation";

            try
            {
                if (model.HotelId > 0)
                {
                    _hotelService.Update(model);
                }
                else
                {
                    _hotelService.Create(model);
                }
                message = "Success";
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(new { model, message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            string message = string.Empty;

            try
            {
                _hotelService.Delete(id);
                message = "Success";
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
    }
}