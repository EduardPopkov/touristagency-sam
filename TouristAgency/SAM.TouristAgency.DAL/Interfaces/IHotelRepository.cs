﻿using SAM.TouristAgency.DAL.Entities;
using System.Linq;

namespace SAM.TouristAgency.DAL.Interfaces
{
    public interface IHotelRepository : IRepository<Hotel>
    {
        IQueryable<Hotel> GetTempHotels(string sortType, int pageSize, int currentPage);

        void Update(Hotel entity);

        bool IsExist(string name);
    }
}
