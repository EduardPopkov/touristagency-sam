﻿using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.Model.ViewModels.Hotel
{
    public class HotelVM
    {
        public int HotelId { get; set; }

        [Required(ErrorMessage = "Name not specified")]
        [StringLength(70, ErrorMessage = "The length of the Name must not exceed 70 characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Class not specified")]
        [Range(1, 5, ErrorMessage = "The Class field must have a value between 1 and 5.")]
        public int Class { get; set; }

        [Range(1, 5000, ErrorMessage = "The Capacity field must have a value between 1 and 5000.")]
        public int Capacity { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "The length of the Name must not exceed 60 characters.")]
        public string Food { get; set; }

        [Required(ErrorMessage = "GeoLong not specified")]
        public decimal GeoLong { get; set; }

        [Required(ErrorMessage = "GeoLat not specified")]
        public decimal GeoLat { get; set; }

        public bool FlagImage { get; set; }
    }
}
