﻿using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using System.Data.Entity;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class TourRepository : Repository<Tour>, ITourRepository
    {
        private readonly DatabaseContext _context;

        public TourRepository(DatabaseContext context)
            : base(context)
        {
            _context = context;
        }

        public IQueryable<Tour> GetTempTours(string sortType, int pageSize, int currentPage)
        {
            IQueryable<Tour> tours = null;

            switch (sortType)
            {
                case "Name ▼":
                    tours = _context.Tours.OrderBy(x => x.Name)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "Name ▲":
                    tours = _context.Tours.OrderByDescending(x => x.Name)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "DateStart ▼":
                    tours = _context.Tours.OrderBy(x => x.DateStart)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "DateStart ▲":
                    tours = _context.Tours.OrderByDescending(x => x.DateStart)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "Duration ▼":
                    tours = _context.Tours.OrderBy(x => x.Duration)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "Duration ▲":
                    tours = _context.Tours.OrderByDescending(x => x.Duration)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "Cost ▼":
                    tours = _context.Tours.OrderBy(x => x.Cost)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                case "Cost ▲":
                    tours = _context.Tours.OrderByDescending(x => x.Cost)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
                default:
                    tours = _context.Tours.OrderBy(x => x.Name)
                .Skip((currentPage - 1) * pageSize).Take(pageSize).Include(x => x.Hotel);
                    break;
            }

            return tours;
        }

        public bool IsExist(string name, int hotelId)
        {
            return _context.Tours
                .Any(x => x.Name.Equals(name) && x.HotelId == hotelId);
        }

        public void Update(Tour entity)
        {
            Tour tour = _context.Tours
                .Where(x => x.TourId == entity.TourId)
                .FirstOrDefault();

            tour.Name = entity.Name;
            tour.Cost = entity.Cost;
            tour.Country = entity.Country;
            tour.DateStart = entity.DateStart;
            tour.Duration = entity.Duration;
            tour.HotelId = entity.HotelId;
        }
    }
}
