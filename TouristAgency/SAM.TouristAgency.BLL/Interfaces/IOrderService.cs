﻿using SAM.TouristAgency.Model.ViewModels.Order;
using System.Collections.Generic;

namespace SAM.TouristAgency.BLL.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<OrderVM> GetAllByAccountId(int accountId);

        OrderVM GetOrder(int id);

        void NewOrder(BookingVM model);

        void CancelOrder(int id);
    }
}
