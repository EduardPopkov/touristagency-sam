﻿let array = new Array();
let allCount = 0;
let SearchArray = new Array();

let GetDate = function (date) {
    var date = parseInt(date.replace(/\D+/g, ""));

    date = new Date(date);

    var year = date.getFullYear();
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);

    var dateObj = { y: year, m: month, d: day };

    return dateObj;
}

$(document).ready(function () {
    FirstPage(SearchArray);
});

let Search = function (control, index) {
    SearchArray = new Array();

    if ($(control).val().length > 0) {

        for (let i = 0; i < array.length; i++) {
            switch (index) {
                case 1:
                    let o = GetDate(array[i].DateStart);
                    let date = o.d + '-' + o.m + '-' + o.y;

                    if (date == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 2:
                    if (array[i].Country == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 3:
                    if (array[i].Duration == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 4:
                    if (array[i].Cost == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
            }
        }
    }
    else {
        SearchArray = array;
    }

    FirstPage(SearchArray);
}

$("#btnFirst").click(function () {
    FirstPage(SearchArray);
});

let FirstPage = function (TempArray) {
    let PageSize = $("#dropPageSize").val();
    let SortType = "Name ▼";
    let CurrentPage = 1;

    $.getJSON("/Home/GetPaggedDataTour?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalPage = 0;
            let rowData = "";

            allCount = response.allCount;
            array = response.temp;

            if (TempArray.length != 0) {
                if (TempArray.length <= PageSize) {
                    TotalPage = Math.ceil(allCount / PageSize);
                }
                else {
                    TotalRecords = TempArray.length;
                    TotalPage = Math.ceil(TotalRecords / PageSize);
                }

                if (PageSize > TempArray.length)
                    PageSize = TempArray.length;

                rowData = GetRows(TempArray, 0, PageSize);
            }
            else {
                TotalPage = Math.ceil(allCount / PageSize);
                rowData = GetRows(array, 0, PageSize);
            }

            $("#txbTotalPage").val(TotalPage);
            $("#TourDiv").empty();
            $("#TourDiv").append(rowData);
            $("#txbCurrentPage").val("1");
        });
}


$("#btnNext").click(function () {
    NextPage();
});

let NextPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "Name ▼";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    CurrentPage++;

    if (CurrentPage <= TotalPage) {
        $.getJSON("/Home/GetPaggedDataTour?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.temp;

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#TourDiv").empty();
                $("#TourDiv").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnPre").click(function () {
    PreviousPage();
});

let PreviousPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "Name ▼";
    let CurrentPage = $("#txbCurrentPage").val();

    CurrentPage--;

    if (CurrentPage >= 1) {
        $.getJSON("/Home/GetPaggedDataTour?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.temp;

                let TotalRecords = response.allCount;

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#TourDiv").empty();
                $("#TourDiv").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnLast").click(function () {
    LastPage();
});

let LastPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "Name ▼";
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    if (CurrentPage != TotalPage) {
        $.getJSON("/Home/GetPaggedDataTour?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + TotalPage,
            {},
            function (response) {
                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#TourDiv").empty();
                $("#TourDiv").append(rowData);
                $("#txbCurrentPage").val(TotalPage);
                $("#txbTotalPage").val(TotalPage);
            });
    }
}

$("#dropPageSize").change(function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = "Name ▼";
    let CurrentPage = $("#txbCurrentPage").val();

    $.getJSON("/Home/GetPaggedDataTour?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = response.allCount;
            let TotalPage = Math.ceil(TotalRecords / PageSize);

            if (CurrentPage > TotalPage)
                CurrentPage = TotalPage;

            let rowData = GetRows(response.temp, 0, response.temp.length);

            $("#TourDiv").empty();
            $("#TourDiv").append(rowData);
            $("#txbCurrentPage").val(CurrentPage);
            $("#txbTotalPage").val(TotalPage);
        });
});

let GetRows = function (array, startIndex, endIndex) {
    let rowData = "";
    let imagePath = "";

    for (let i = startIndex; i < endIndex; i++) {
        try {

            imagePath = "data:image/jpeg;base64," + array[i].ImagePath;

            let d = GetDate(array[i].DateStart);

            rowData = rowData + '<div class="grow row no-gutters shadow bg-white rounded my-2">' +
                '<div class="col-md-4 ">' +
                '<img src="' + imagePath + '" class="card-img" height="240" alt="...">' +
                '</div>' +
                '<div class="col-md-8">' +
                '<div class="card-body">' +
                '<h3 class="card-title">' + array[i].Name + '</h3>' +
                '<p class="card-text"><strong>Cost: </strong>' + array[i].Cost + '$<strong class="ml-2"> Country: </strong >' + array[i].Country + '</p>' +
                '<p class="card-text"><strong>Duration: </strong>' + array[i].Duration + '</p> ' +
                '<p class="card-text"><strong>Date Start: </strong>' + d.d + '-' + d.m + '-' + d.y + '</p>' +
                '<a href="/Home/HotelDetail?hotelId=' + array[i].HotelId + '&tourId=' + array[i].TourId + '" class="btn btn-outline-info">Show Hotel</a>' +
                '<a href="/Home/TourDetail?tourId=' + array[i].TourId + '" class="btn btn-outline-success mx-1">Booking</a>' +
                '</div></div></div>';

        }
        catch (e) {
        }
    }

    return rowData;
}