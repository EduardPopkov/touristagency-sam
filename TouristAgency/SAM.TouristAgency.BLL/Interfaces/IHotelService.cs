﻿using SAM.TouristAgency.Model.ViewModels.Hotel;
using System.Linq;

namespace SAM.TouristAgency.BLL.Interfaces
{
    public interface IHotelService
    {
        IQueryable<HotelComboBoxVM> GetAll();

        IQueryable<HotelVM> GetTempHotels(string sortType, int pageSize, int currentPage);

        HotelVM GetById(int id);

        void Create(HotelVM model);

        void Update(HotelVM model);

        void Delete(int id);

        int Count();

        bool IsExist(string name);
    }
}
