﻿let array = new Array();
let allCount = 0;
let SearchArray = new Array();

let GetDate = function (date) {
    var date = parseInt(date.replace(/\D+/g, ""));

    date = new Date(date);

    var year = date.getFullYear();
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);

    var dateObj = { y: year, m: month, d: day };

    return dateObj;
}

$(document).ready(function () {
    FirstPage(SearchArray);
});

let Search = function (control, index) {
    SearchArray = new Array();

    if ($(control).val().length > 0) {

        for (let i = 0; i < array.length; i++) {
            switch (index) {
                case 0:
                    if (array[i].Name == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 1:
                    let o = GetDate(array[i].DateStart);
                    let date = o.d + '-' + o.m + '-' + o.y;

                    if (date == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 2:
                    if (array[i].Duration == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 3:
                    if (array[i].Cost == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
                case 4:
                    if (array[i].Country == $(control).val()) {
                        SearchArray.push(array[i]);
                    }
                    break;
               
            }
        }
    }
    else {
        SearchArray = array;
    }

    FirstPage(SearchArray);
}

$('th a').on('click', function () {
    let PageSize = $("#dropPageSize").val();
    let CurrentPage = $("#txbCurrentPage").val();

    $(this).text($(this).text().includes(' ▼') ?
        $(this).text().replace(" ▼", " ▲") :
        $(this).text().replace(' ▲', " ▼"));

    $.getJSON("/Admin/Tour/GetPaggedData?sortType=" + $(this).text() + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            array = response.temp;

            let rowData = GetRows(array, 0, array.length);

            $("#TourTable").empty();
            $("#TourTable").append(rowData);
        });
});

$("#btnFirst").click(function () {
    FirstPage(SearchArray);
});

let FirstPage = function (TempArray) {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = 1;

    $.getJSON("/Admin/Tour/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = 0;
            let TotalPage = 0;
            let rowData = "";

            allCount = response.allCount;
            array = response.temp;

            if (TempArray.length != 0) {
                if (TempArray.length <= PageSize) {
                    TotalPage = Math.ceil(allCount / PageSize);
                }
                else {
                    TotalRecords = TempArray.length;
                    TotalPage = Math.ceil(TotalRecords / PageSize);
                }

                if (PageSize > TempArray.length)
                    PageSize = TempArray.length;

                rowData = GetRows(TempArray, 0, PageSize);
            }
            else {
                TotalPage = Math.ceil(allCount / PageSize);
                rowData = GetRows(array, 0, PageSize);
            }

            $("#txbTotalPage").val(TotalPage);
            $("#TourTable").empty();
            $("#TourTable").append(rowData);
            $("#txbCurrentPage").val("1");
        });
}


$("#btnNext").click(function () {
    NextPage();
});

let NextPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    CurrentPage++;

    if (CurrentPage <= TotalPage) {
        $.getJSON("/Admin/Tour/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.temp;

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#TourTable").empty();
                $("#TourTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnPre").click(function () {
    PreviousPage();
});

let PreviousPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();

    CurrentPage--;

    if (CurrentPage >= 1) {
        $.getJSON("/Admin/Tour/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
            {},
            function (response) {
                array = response.temp;

                let TotalRecords = response.allCount;

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#TourTable").empty();
                $("#TourTable").append(rowData);
                $("#txbCurrentPage").val(CurrentPage);
            });
    }
}

$("#btnLast").click(function () {
    LastPage();
});

let LastPage = function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();
    let TotalPage = $("#txbTotalPage").val();

    if (CurrentPage != TotalPage) {
        $.getJSON("/Admin/Tour/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + TotalPage,
            {},
            function (response) {

                let rowData = GetRows(response.temp, 0, response.temp.length);

                $("#TourTable").empty();
                $("#TourTable").append(rowData);
                $("#txbCurrentPage").val(TotalPage);
                $("#txbTotalPage").val(TotalPage);
            });
    }
}

$("#dropPageSize").change(function () {
    let PageSize = $("#dropPageSize").val();
    let SortType = $("#sortName").text();
    let CurrentPage = $("#txbCurrentPage").val();

    $.getJSON("/Admin/Tour/GetPaggedData?sortType=" + SortType + "&pageSize=" + PageSize + "&currentPage=" + CurrentPage,
        {},
        function (response) {
            let TotalRecords = response.allCount;
            let TotalPage = Math.ceil(TotalRecords / PageSize);

            if (CurrentPage > TotalPage)
                CurrentPage = TotalPage;

            let rowData = GetRows(response.temp, 0, response.temp.length);

            $("#TourTable").empty();
            $("#TourTable").append(rowData);
            $("#txbCurrentPage").val(CurrentPage);
            $("#txbTotalPage").val(TotalPage);
        });
});

let AddNew = function () {
    $("#myForm")[0].reset();
    $("#MyModal").modal();
}

let DeleteRecord = function (TourId) {
    $("#TourId").val(TourId);
    $("#DeleteConfirmation").modal("show");
}

let ConfirmDelete = function () {
    let TourId = $("#TourId").val();

    $.ajax({
        type: "POST",
        url: "/Admin/Tour/Delete?id=" + TourId,
        success: function (result) {
            if (result == "Success") {
                alert(result);
                $("#DeleteConfirmation").modal("hide");
                window.location.href = "/Admin/Tour/Index";
            }
            else {
                alert(result);
            }
        }
    })
}

let GetRows = function (array, startIndex, endIndex) {
    let rowData = "";
    let imagePath = "";

    for (let i = startIndex; i < endIndex; i++) {
        try {
            imagePath = "data:image/jpeg;base64," + array[i].ImagePath;

            let d = GetDate(array[i].DateStart);

            rowData = rowData + '<tr data-rowid="' + array[i].TourId + '">' +
                '<td><img width="100" height="50" src="' + imagePath + '"/></td>' +
                '<td>' + array[i].Name + '</td>' +
                '<td>' + d.d + '-' + d.m + '-' + d.y +  '</td>' +
                '<td>' + array[i].Duration + '</td>' +
                '<td>' + array[i].Cost + '</td>' +
                '<td>' + array[i].Country + '</td>' +
                '<td>' + array[i].HotelName + '</td>' +
                '<td><a href="#" class="btn btn-danger" onclick="DeleteRecord(' + array[i].TourId + ')">Delete</a></td>' +
                '</tr>';
        }
        catch (e) {
            //console.log(e);
        }
    }

    return rowData;
}