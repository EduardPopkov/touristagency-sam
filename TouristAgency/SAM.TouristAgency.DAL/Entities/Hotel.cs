﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.DAL.Entities
{
    public class Hotel
    {
        public Hotel()
        {
            Images = new HashSet<HotelImage>();
            Tours = new HashSet<Tour>();
        }

        [Key]
        public int HotelId { get; set; }

        [Required]
        [StringLength(70)]
        public string Name { get; set; }

        [Required]
        public int Class { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        [StringLength(60)]
        public string Food { get; set; }

        [Required]
        public decimal GeoLat { get; set; }

        [Required]
        public decimal GeoLong { get; set; }

        public virtual ICollection<HotelImage> Images { get; set; }
        public virtual ICollection<Tour> Tours { get; set; }
    }
}
