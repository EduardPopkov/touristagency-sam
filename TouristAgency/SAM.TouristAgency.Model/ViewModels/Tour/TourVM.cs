﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.Model.ViewModels.Tour
{
    public class TourVM
    {
        public int TourId { get; set; }

        [Required(ErrorMessage = "Name not specified")]
        [StringLength(70, ErrorMessage = "The length of the Name must not exceed 70 characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "DateStart not specified")]
        [DataType(DataType.Date)]
        public DateTime DateStart { get; set; }

        [Required(ErrorMessage = "Country not specified")]
        [StringLength(70, ErrorMessage = "The length of the Country must not exceed 70 characters.")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Duration not specified")]
        [Range(1, 20, ErrorMessage = "The Duration field must have a value between 1 and 20.")]
        public int Duration { get; set; }

        [Required(ErrorMessage = "Cost not specified")]
        [Range(0.1, 1000000, ErrorMessage = "The Cost field must have a value between 0.1 and 1000000.")]
        public decimal Cost { get; set; }

        [Required(ErrorMessage = "Hotel not specified")]
        public int HotelId { get; set; }

        [Required(ErrorMessage = "Description not specified")]
        [StringLength(200, ErrorMessage = "The length of the Description must not exceed 200 characters.")]
        public string Description { get; set; }

        public byte[] Image { get; set; }

        public string ImagePath { get; set; }

        public string HotelName { get; set; }
        public int HotelClass { get; set; }
        public int HotelCapacity { get; set; }
        public string HotelFood { get; set; }
    }
}
