﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Comment;
using System;
using System.Linq;

namespace SAM.TouristAgency.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CommentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(CommentVM model)
        {
            _unitOfWork.Comments
                .Add(new Comment
                {
                    AccountId = model.AccountId,
                    TourId = model.TourId,
                    Message = model.Message,
                    DateAdded = model.DateAdded,
                    IsRight = false
                });

            _unitOfWork.Save();
        }

        public int Count()
        {
            return _unitOfWork.Comments
                .Count();
        }

        public void Delete(int id)
        {
            Comment comment = _unitOfWork.Comments
                .GetById(id);

            if (comment == null)
            {
                throw new ApplicationException($"Can't find comment with id = {id}");
            }

            _unitOfWork.Comments
                .Delete(comment);

            _unitOfWork.Save();
        }

        public CommentVM GetById(int id)
        {
            Comment comment = _unitOfWork.Comments
                .GetById(id);

            if (comment == null)
            {
                throw new ApplicationException($"Can't find comment with id = {id}");
            }

            return new CommentVM
            {
                TourId = comment.TourId,
                AccountId = comment.AccountId,
                DateAdded = comment.DateAdded,
                IsRight = comment.IsRight,
                Message = comment.Message,
                CommentId = comment.CommentId,
                AccountName = comment.Account.Email,
                TourName = comment.Tour.Name
            };
        }

        public IQueryable<CommentVM> GetTempAllComments(string sortType, int pageSize, int currentPage)
        {
            return _unitOfWork.Comments
                .GetTempAllComments(sortType, pageSize, currentPage)
                .Select(x => new CommentVM
                {
                    CommentId = x.CommentId,
                    AccountId = x.AccountId,
                    TourId = x.TourId,
                    Message = x.Message,
                    DateAdded = x.DateAdded,
                    AccountName = x.Account.Email,
                    TourName = x.Tour.Name
                });
        }

        public IQueryable<CommentVM> GetTempByTourId(int tourId)
        {
            return _unitOfWork.Comments
                .GetTempByTourId(tourId)
                .Select(x => new CommentVM
                {
                    CommentId = x.CommentId,
                    AccountId = x.AccountId,
                    TourId = x.TourId,
                    Message = x.Message,
                    DateAdded = x.DateAdded,
                    AccountName = x.Account.Email,
                    TourName = x.Tour.Name
                });
        }

        public IQueryable<CommentVM> GetTempIsNotRightComments(string sortType, int pageSize, int currentPage)
        {
            return _unitOfWork.Comments
                .GetTempIsNotRightComments(sortType, pageSize, currentPage)
                .Select(x => new CommentVM
                {
                    CommentId = x.CommentId,
                    AccountId = x.AccountId,
                    TourId = x.TourId,
                    Message = x.Message,
                    DateAdded = x.DateAdded,
                    AccountName = x.Account.Email,
                    TourName = x.Tour.Name
                });
        }

        public void Update(CommentVM entity)
        {
            Comment comment = _unitOfWork.Comments
                .GetById(entity.CommentId);

            if (comment == null)
            {
                throw new ApplicationException($"Can't find comment with id = {entity.CommentId}");
            }

            comment.IsRight = true;

            _unitOfWork.Save();
        }
    }
}
