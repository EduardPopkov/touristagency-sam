﻿$('#defaultCheck1').on('change', function () {
    if (this.checked) {
        $("#CreditCard").prop("disabled", false);
        $("#Book").prop("disabled", false);
    }
    else {
        $("#CreditCard").prop("disabled", true);
        $("#Book").prop("disabled", true);
    }
});

let CancelBooking = function (OrderId) {
    $("#OrderId").val(OrderId);
    $("#DeleteConfirmation").modal("show");
}

let ConfirmCancel = function () {
    let OrderId = $("#OrderId").val();

    $.ajax({
        type: "POST",
        url: "/User/Order/CancelBooking?orderId=" + OrderId,
        success: function (result) {
            if (result == "Success") {
                alert(result);
                $("#DeleteConfirmation").modal("hide");
                window.location.href = "/User/Account/MyProfile";
            }
            else {
                alert(result);
            }
        }
    })
}
