﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.DAL.Entities
{
    public class Tour
    {
        public Tour()
        {
            Orders = new HashSet<Order>();
            Comments = new HashSet<Comment>();
        }

        [Key]
        public int TourId { get; set; }

        [Required]
        [StringLength(70)]
        public string Name { get; set; }

        [Required]
        public DateTime DateStart { get; set; }

        [Required]
        [StringLength(70)]
        public string Country { get; set; }

        [Required]
        public int Duration { get; set; }

        [Required]
        public decimal Cost { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        public byte[] Image { get; set; }

        [Required]
        public int HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
