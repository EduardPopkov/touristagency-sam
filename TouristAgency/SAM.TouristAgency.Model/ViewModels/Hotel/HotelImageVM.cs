﻿using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.Model.ViewModels.Hotel
{
    public class HotelImageVM
    {
        public int ImageId { get; set; }

        public byte[] Image { get; set; }

        public string ImagePath { get; set; }

        [Required]
        public int HotelId { get; set; }

        public string HotelName { get; set; }
    }
}
