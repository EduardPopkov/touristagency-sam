﻿using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class HotelImageRepository : Repository<HotelImage>, IHotelImageRepository
    {
        private readonly DatabaseContext _context;

        public HotelImageRepository(DatabaseContext context)
          : base(context)
        {
            _context = context;
        }

        public IQueryable<HotelImage> GetByHotelId(int hotelId)
        {
            return _context.HotelImages
                .Where(x => x.HotelId == hotelId);
        }

        public IQueryable<HotelImage> GetTempImage(string sortType, int pageSize, int currentPage)
        {
            IQueryable<HotelImage> hotelImages = _context.HotelImages.OrderBy(x => x.ImageId)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);

            return hotelImages;
        }
    }

}
