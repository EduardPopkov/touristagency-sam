﻿using SAM.TouristAgency.Model.ViewModels.Account;

namespace SAM.TouristAgency.BLL.Interfaces
{
    public interface IAccountService
    {
        AccountVM GetByEmail(string email);

        LoginVM Login(LoginVM model);

        void Registration(RegisterVM model);

        string GetRole(string email);

        bool IsExist(string email);

        bool IsAdmin(string email);
    }
}
