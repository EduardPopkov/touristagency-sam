﻿using Newtonsoft.Json;
using NLog;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Comment;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SAM.TouristAgency.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CommentController : Controller
    {
        readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ICommentService _commentService;
        private readonly IAccountService _accountService;

        public CommentController(ICommentService commentService, IAccountService accountService)
        {
            _commentService = commentService;
            _accountService = accountService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAll()
        {
            return View();
        }

        public JsonResult GetPaggedDataAll(string sortType, int pageSize, int currentPage)
        {
            IQueryable<CommentVM> comments = _commentService.GetTempAllComments(sortType, pageSize, currentPage);

            int allCount = _commentService.Count();

            return Json(new { comments, allCount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPaggedData(string sortType, int pageSize, int currentPage)
        {
            IQueryable<CommentVM> comments = _commentService.GetTempIsNotRightComments(sortType, pageSize, currentPage);

            int allCount = _commentService.Count();

            return Json(new { comments, allCount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetById(int id)
        {
            string message = string.Empty;

            try
            {
                CommentVM comment = _commentService.GetById(id);

                message = JsonConvert.SerializeObject(comment, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(CommentVM model)
        {
            string message = "The form did not pass validation";

            if (ModelState.IsValid)
            {
                try
                {
                    _commentService.Update(model);

                    message = "Success";
                }
                catch (ApplicationException ex)
                {
                    logger.Error(ex.Message);
                    message = ex.Message;
                }
            }

            return RedirectToAction("Index");
        }

        public JsonResult Delete(int id)
        {
            string message = string.Empty;

            try
            {
                _commentService.Delete(id);
                message = "Success";
            }
            catch (ApplicationException ex)
            {
                logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
    }
}