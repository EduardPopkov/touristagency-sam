﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAM.TouristAgency.Model.ViewModels.Account
{
    public class AccountVM
    {
        public int AccountId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
