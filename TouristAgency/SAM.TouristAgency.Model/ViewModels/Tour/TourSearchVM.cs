﻿using System;

namespace SAM.TouristAgency.Model.ViewModels.Tour
{
    public class TourSearchVM
    {
        public string Name { get; set; }

        public DateTime DateStart { get; set; }

        public string Country { get; set; }

        public int Duration { get; set; }

        public decimal Cost { get; set; }
    }
}
