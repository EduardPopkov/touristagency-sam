﻿namespace SAM.TouristAgency.Common.Constants
{
    public static class Role
    {
        public static string AdminRole = "Admin";
        public static string UserRole = "User";
    }
}
