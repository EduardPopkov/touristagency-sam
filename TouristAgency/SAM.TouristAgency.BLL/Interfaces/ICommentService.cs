﻿using SAM.TouristAgency.Model.ViewModels.Comment;
using System.Linq;

namespace SAM.TouristAgency.BLL.Interfaces
{
    public interface ICommentService
    {
        IQueryable<CommentVM> GetTempAllComments(string sortType, int pageSize, int currentPage);

        IQueryable<CommentVM> GetTempIsNotRightComments(string sortType, int pageSize, int currentPage);

        IQueryable<CommentVM> GetTempByTourId(int tourId);

        CommentVM GetById(int id);

        void Add(CommentVM model);

        void Update(CommentVM entity);

        void Delete(int id);

        int Count();
    }
}
