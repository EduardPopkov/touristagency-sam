﻿namespace SAM.TouristAgency.Model.ViewModels.Hotel
{
    public class HotelComboBoxVM
    {
        public int HotelId { get; set; }
        public string Name { get; set; }
    }
}
