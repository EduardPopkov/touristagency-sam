﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Tour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAM.TouristAgency.BLL.Services
{
    public class TourService : ITourService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TourService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int Count()
        {
            return _unitOfWork.Tours
                .Count();
        }

        public void Create(TourVM model)
        {
            bool isExist = _unitOfWork.Tours
                .IsExist(model.Name, model.HotelId);

            if (isExist)
            {
                throw new ApplicationException("Tour with the same name is already exist");
            }

            _unitOfWork.Tours
                .Add(new Tour
                {
                    Name = model.Name,
                    Cost = model.Cost,
                    HotelId = model.HotelId,
                    Country = model.Country,
                    DateStart = model.DateStart,
                    Description = model.Description,
                    Duration = model.Duration,
                    Image = model.Image
                });

            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            Tour tour = _unitOfWork.Tours
                .GetById(id);

            if (tour == null)
            {
                throw new ApplicationException($"Can't find tour with id = {id}");
            }

            _unitOfWork.Tours
                .Delete(tour);

            _unitOfWork.Save();
        }

        public TourVM GetById(int id)
        {
            Tour tour = _unitOfWork.Tours
               .GetById(id);

            if (tour == null)
            {
                throw new ApplicationException($"Can't find tour with id = {id}");
            }

            return new TourVM
            {
                TourId = tour.TourId,
                Image = tour.Image,
                Duration = tour.Duration,
                Cost = tour.Cost,
                Country = tour.Country,
                DateStart = tour.DateStart,
                Description = tour.Description,
                Name = tour.Name,
                HotelName = tour.Hotel.Name,
                HotelId = tour.HotelId,
                HotelCapacity = tour.Hotel.Capacity,
                HotelClass = tour.Hotel.Class,
                HotelFood = tour.Hotel.Food
            };
        }

        public IQueryable<TourVM> GetTempTours(string sortType, int pageSize, int currentPage)
        {
            IQueryable<TourVM> tours = _unitOfWork.Tours
                .GetTempTours(sortType, pageSize, currentPage)
                .Select(tour => new TourVM
                {
                    TourId = tour.TourId,
                    Image = tour.Image,
                    Duration = tour.Duration,
                    Cost = tour.Cost,
                    Country = tour.Country,
                    DateStart = tour.DateStart,
                    Description = tour.Description,
                    Name = tour.Name,
                    HotelName = tour.Hotel.Name,
                    HotelId = tour.HotelId
                });

            return tours;
        }
    }
}
