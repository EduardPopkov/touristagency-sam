﻿using Microsoft.Practices.Unity;
using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.BLL.Services;
using SAM.TouristAgency.DAL.Interfaces;
using SAM.TouristAgency.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SAM.TouristAgency.IoC.Dependencies
{
    public class MyDependencyResolver : IDependencyResolver
    {
        readonly IUnityContainer _container;

        public MyDependencyResolver()
        {
            _container = new UnityContainer();

            _container.RegisterType<IUnitOfWork, UnitOfWork>();
            _container.RegisterType<IHotelService, HotelService>();
            _container.RegisterType<IAccountService, AccountService>();
            _container.RegisterType<IHotelImageService, HotelImageService>();
            _container.RegisterType<ITourService, TourService>();
            _container.RegisterType<ICommentService, CommentService>();
            _container.RegisterType<IOrderService, OrderService>();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return _container.Resolve(serviceType);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return _container.ResolveAll(serviceType);
            }
            catch
            {
                return null;
            }
        }
    }
}
