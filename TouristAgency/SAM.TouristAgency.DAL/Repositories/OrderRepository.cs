﻿using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        private readonly DatabaseContext _context;

        public OrderRepository(DatabaseContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<Order> GetAllByAccountId(int accountId)
        {
            return _context.Orders
                .Where(x => x.AccountId == accountId);
        }
    }
}
