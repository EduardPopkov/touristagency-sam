﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAM.TouristAgency.DAL.Entities
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public decimal Cost { get; set; }

        public string Name { get; set; }

        [Required]
        [StringLength(9)]
        public string Passport { get; set; }

        [Required]
        public DateTime DateIssued { get; set; }

        [Required]
        [StringLength(16)]
        public string CreditCard { get; set; }

        [Required]
        public DateTime DateOrder { get; set; }

        public int TourId { get; set; }
        public int AccountId { get; set; }

        public virtual Account Account { get; set; }
        public virtual Tour Tour { get; set; }
    }
}
