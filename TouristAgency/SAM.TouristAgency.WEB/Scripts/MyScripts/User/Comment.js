﻿let AddNew = function () {
    $("#myForm")[0].reset();
    $("#ModalTitle").html("Add Comment Record");
    $("#MyModal").modal();
}

let AddComment = function (form) {
    var elems = form.elements;

    console.log(elems.Message.value);

    $.ajax({
        type: "POST",
        url: form.action,
        dataType: "json",
        data: {
            accountId: elems.AccountId.value,
            tourId: elems.TourId.value,
            message: elems.Message.value
        },
        success: function (data) {
            alert(data);
            $("#MyModal").modal("hide");
        }
    });

}