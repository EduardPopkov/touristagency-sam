﻿using SAM.TouristAgency.DAL.Entities;
using SAM.TouristAgency.DAL.Interfaces;
using System.Linq;

namespace SAM.TouristAgency.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        private readonly DatabaseContext _context;

        public CommentRepository(DatabaseContext context)
            : base(context)
        {
            _context = context;
        }

        public IQueryable<Comment> GetTempAllComments(string sortType, int pageSize, int currentPage)
        {
            IQueryable<Comment> comments = _context.Comments
                .OrderBy(x => x.DateAdded)
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize);

            return comments;
        }

        public IQueryable<Comment> GetTempByTourId(int tourId)
        {
            IQueryable<Comment> comments = _context.Comments
                .Where(x => x.TourId == tourId && x.IsRight == true);

            return comments;
        }

        public IQueryable<Comment> GetTempIsNotRightComments(string sortType, int pageSize, int currentPage)
        {
            IQueryable<Comment> comments = _context.Comments
                .Where(x => x.IsRight == false)
                .OrderBy(x => x.DateAdded)
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize);

            return comments;
        }

        public void Update(Comment entity)
        {
            Comment comment = _context.Comments
               .Where(x => x.CommentId == entity.CommentId)
               .FirstOrDefault();

            comment.IsRight = comment.IsRight;
        }
    }
}
