﻿using SAM.TouristAgency.BLL.Interfaces;
using SAM.TouristAgency.Model.ViewModels.Hotel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAM.TouristAgency.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HotelImageController : Controller
    {
        private readonly IHotelImageService _hotelImageService;
        private readonly IHotelService _hotelService;

        public HotelImageController(IHotelImageService hotelImageService, IHotelService hotelService)
        {
            _hotelImageService = hotelImageService;
            _hotelService = hotelService;
        }

        public ActionResult Index()
        {
            ViewBag.HotelList = _hotelService.GetAll();

            return View();
        }

        public JsonResult GetPaggedData(string sortType, int pageSize, int currentPage)
        {
            IEnumerable<HotelImageVM> pictures = _hotelImageService.GetTempHotels(sortType, pageSize, currentPage);

            var temp = from p in pictures
                       select new HotelImageVM
                       {
                           ImageId = p.ImageId,
                           ImagePath = Convert.ToBase64String(p.Image),
                           HotelId = p.HotelId,
                           HotelName = p.HotelName
                       };

            int allCount = _hotelImageService.Count();

            JsonResult json = Json(new { temp, allCount }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        public ActionResult AddOrEdit(HotelImageVM model, HttpPostedFileBase UploadImage)
        {
            if (ModelState.IsValid && UploadImage != null)
            {
                byte[] imageData = null;

                using (var binaryReader = new BinaryReader(UploadImage.InputStream))
                {
                    imageData = binaryReader.ReadBytes(UploadImage.ContentLength);
                }

                model.Image = imageData;

                _hotelImageService.Create(model);
            }

            return RedirectToAction("Index");
        }

        public JsonResult Delete(int id)
        {
            string message = string.Empty;

            try
            {
                _hotelImageService.Delete(id);
                message = "Success";
            }
            catch (ApplicationException ex)
            {
                //logger.Error(ex.Message);
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
    }
}